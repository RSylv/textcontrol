import { banWords, goodWords } from './_wordsArray.js'

// Declare the const variable (Dom Element)
const textarea = document.querySelector('textarea');
const btn = document.querySelector('.btn-send');
const result = document.getElementById('result');
const span = document.querySelector('.length');
const switchOnInput = document.getElementById('switch');


// on input into textarea, do that stuff
textarea.addEventListener('input', () => {

    // if switchOnInput is checked
    if (switchOnInput.checked) {

        // get the length of the text and display it in span under textarea
        span.textContent = textarea.value.length

        // go to function to show the instant result
        scanAndChangeWords()
    }

})

// if click on send, go to 'scanAndChangeWords' function
btn.addEventListener('click', scanAndChangeWords)



/**
 * Scan a text and change words if needed.
 * We have to have too array of words (bad and good).
 * If a bad word is present in the text, it is change whith a random good word.
 */
function scanAndChangeWords() {

    // Get the text in array 
    const text = textarea.value.trim().split(" ")

    // if nothing transmit send text message
    if (text === '') {
        result.textContent = 'Rein a analyser ..'
    }
    // else
    else {

        // Scan banWords arrays
        for (let i = 0; i < text.length; i++) {

            // no white space before and after
            text[i] = text[i].trim()

            // word to lower case
            text[i] = text[i].toLocaleLowerCase()

            // Exceptions
            switch (text[i]) {
                case "mere":
                case "mère":
                    text[i] = "maman"
                    break;
                case "meres":
                case "mères":
                    text[i] = "mamans"
                    break;
                default:
                    break;
            }

            // Otherwise Compare words and change it if needed
            if (banWords.includes(text[i])) {

                // get a random word in goodWords array
                let randomGoodWord = goodWords[Math.floor(Math.random() * goodWords.length)]

                // change the bad text word with the good word
                text[i] = randomGoodWord
            }
        }

        // Get the new text
        const newText = text.toString().replace(/,/gi, " ")

        // Append the text
        result.textContent = newText
    }
}
