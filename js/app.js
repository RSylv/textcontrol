// Declare the const variable (Dom Element)
const textarea = document.querySelector('textarea');
const btn = document.querySelector('.btn-send');
const result = document.getElementById('result');
const span = document.querySelector('.length');
const switchOnInput = document.getElementById('switch');

// Get the datas.json
fetch("./js/DATA/_wordsArray.json")
    .then(res => res.json())
    .then(data => { init(data) })
    .catch(err => console.warn(err))

// Start
function init(data) {

    // Ready
    console.log("data's ready");

    // on input into textarea, do that stuff
    textarea.addEventListener('input', () => {

        // if switchOnInput is checked
        if (switchOnInput.checked) {

            // get the length of the text and display it in span under textarea
            span.textContent = textarea.value.length

            // go to function to show the instant result
            scanAndChangeWords(data)
        }

    })

    // if click on send, go to 'scanAndChangeWords' function
    btn.addEventListener('click', () => {
        scanAndChangeWords(data)
    })

}

/**
 * Scan a text and change words if needed.
 * We have to have 4 arrays of words (banwords, goodwords, exceptions and expressions).
 * If a bad word is present in the text, it is change whith a random good word.
 */
function scanAndChangeWords(data) {

    // if nothing transmit send text message
    if (textarea.value.trim() === '') {
        result.textContent = 'Rien a analyser ..'
    }
    // else
    else {

        // Get the text in array 
        const text = textarea.value.trim().split(" ")

        // Scan banwords arrays
        for (let i = 0; i < text.length; i++) {

            // NORMALIZER
            text[i] = normalizeWord(text[i])

            // SPECIAL CHARS REPLACER
            if (text[i].indexOf('<') !== -1 || text[i].indexOf('>') !== -1) {
                text[i] = indexesAndReplace(text[i].split(""), "<", " ♫ ");
                text[i] = indexesAndReplace(text[i].split(""), ">", " ♪ ");
            }
            if (text[i].indexOf('$') !== -1 || text[i].indexOf('_') !== -1) {
                text[i] = indexesAndReplace(text[i].split(""), "$", " ♥ ");
                text[i] = indexesAndReplace(text[i].split(""), "_", " ");
            }            

            // EXCEPTIONS ARRAY
            data.exceptions.forEach(d => {
                // Singulier
                text[i] === d.word ? text[i] = d.replacer : '';
                // Pluriel
                text[i] === d.word+'s' ? text[i] = d.replacer+'s' : '';
            });

            // EXPRESSIONS ARRAY
            data.expressions.forEach(d => {
                phraseReplace(text, i, d.phrase, d.replacer)
            })
 
            // BANWORDS ARRAY & GOODWORDS ARRAY
            if (data.banwords.includes(text[i]) || data.banwords.includes(text[i].substr(0, text[i].length-1))) {

                // get a random word in goodwords array
                let randomGoodWord = data.goodwords[Math.floor(Math.random() * data.goodwords.length)]

                // change the bad text word with the good word
                text[i] = randomGoodWord
            }
        }

        // Capitalize first letter
        text[0] = text[0][0].toUpperCase() + text[0].slice(1, text[0].length)

        // Get the new text, replace , and .
        const newText = text.toString().replace(/,/gi, " ").replace(/\./g, ". \n\r")

        // Append the text
        result.innerText = newText
    }
}



const normalizeWord = (word) => {
    // no white space before and after
    word = word.trim()
    // word to lower case
    word = word.toLocaleLowerCase()
    // delete point
    word = word.replace(/\./g, "")
    console.log(word);
    // Remove accent, normalize str
    return word.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
}

const indexesAndReplace = (textArr, search, replacer) => {
    const res = textArr.reduce((acc, val, idx) => (val === search && acc.push(idx), acc), [])
    res.map(x => textArr[x] = replacer);
    return textArr.toString().replace(/,/gi, "");
}

const phraseReplace = (text, i, search, replacer) => {
    const word = search.split(" ")
    if (
        text[i] === word[0] &&
        text[i + 1] === word[1] &&
        text[i + 2] === word[2]) {
        text[i + 1] = ""
        text[i + 2] = ""
        text[i] = replacer
    }
}