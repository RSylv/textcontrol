<?php

define('JSON_FILE_PATH', './js/DATA/_wordsArray.json');

/**
 * METHOD POST ONLY
 */
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    /**
     * Check Form type
     * 
     * - SAVE 
     * - DELETE 
     * - exit
     */
    if ($_POST['type'] === 'save') :

        // Create data array 
        $_POST['word_arrays'] !== 'banwords' && $_POST['word_arrays'] !== 'goodwords' ?
            $data = array(
                "array" => $_POST['word_arrays'],
                "word" => strtolower(trim($_POST['word'])),
                "replacer" => strtolower(trim($_POST['replacer'])),
            ) :
            $data = array(
                "array" => $_POST['word_arrays'],
                "word" => strtolower(trim($_POST['word'])),
            );

        // Validate form
        $cleanData = validateForm($data);

        // If true, save it.
        if ($cleanData) :
            saveDataToFile($cleanData);
        endif;

    elseif ($_POST['type'] === 'delete') :

        if (!is_numeric($_POST['index'])) {
            http_response_code(403);
            exit();
        }

        $data = array(
            "word" => strtolower(trim($_POST['word'])),
            "index" => (int)$_POST['index'],
            "word_array" => strtolower(trim($_POST['word_array'])),
        );

        if (deleteDataToFile($data)) {
            print_r(json_encode(array("message" => "L'entrée a bien été éfacée !")));
            exit();
        } else {
            print_r(json_encode(array("message" => "erreur")));
            exit();
        }

    else :
        http_response_code(400);
        exit();
    endif;

    // Get out if arrived here
    http_response_code(400);
    exit();
}
// IF NOT POST METHOD, Return Home 
else {
    header("Location: ./index.html");
    exit();
};



/**
 * Check if it exist, and sanitise the data.
 *
 * @param string $data
 * @return boolean - data or false
 */
function sanitize(string $data = '')
{
    if ((!isset($data) && empty($data))) {
        return false;
    } else {
        $data = htmlspecialchars($data);
        $data = htmlentities($data);
        $data = strip_tags($data);
        $data = stripslashes($data);
        $data = trim($data);

        return $data;
    }
}


function validateForm(array $data = [])
{
    $valid = true;

    // Sanitize
    foreach ($data as $d) {
        $res = sanitize($d);
        if (!$res) {
            $valid = false;
            print_r(json_encode(array("message" => "Vous devez remplir les champs.")));
            exit();
        }
    }

    // Verify
    switch ($data["array"]) {
        case 'banwords':
        case 'goodwords':
            // Get the first word
            $data["word"] = explode(" ", $data["word"])[0];

            // Verify lenght of it
            if (25 < strlen($data["word"]) || 2 > strlen($data["word"])) {
                print_r(json_encode(array("message" => "Problème de longueur du mot. (Min.2 / Max.25)")));
                $valid = false;
                exit();
            }
            break;
        case 'exceptions':
            $data["word"] = explode(" ", $data["word"])[0];
            $data["replacer"] = explode(" ", $data["replacer"])[0];
            if (
                25 < strlen($data["word"]) || 2 > strlen($data["word"]) ||
                25 < strlen($data["replacer"]) || 2 > strlen($data["replacer"])
            ) {
                print_r(json_encode(array("message" => "Problème de longueur du mot. (Min.2 / Max.25)")));
                $valid = false;
                exit();
            }
            break;
        case 'expressions':

            // Explode string in word array (separator is space)
            $res = explode(" ", $data["word"]);

            $data["phrase"] = "";
            (int)$lenght = 0;

            // Three words maximum
            count($res) <= 3 ? $lenght = count($res) : $lenght = 3;

            // Get only the first three words, in new key (relative to json array format.)
            for ($i = 0; $i < $lenght; $i++) {
                if (isset($res[$i]) && !empty($res[$i])) {
                    $data["phrase"] .= $res[$i] . ' ';
                }
            }

            $data["phrase"] = trim($data["phrase"]);


            // Verify lenght of each word
            foreach (explode(" ", $data["phrase"]) as $word) {
                if (
                    25 < strlen($word) || 2 > strlen($word)
                ) {
                    print_r(json_encode(array("message" => "Problème de longueur du mot. (Min.2 / Max.25) / words")));
                    $valid = false;
                    exit();
                }
            }

            // delete key/value word.
            unset($data["word"]);

            break;

        default:
            http_response_code(403);
            exit();
            break;
    }

    // Return statement
    if ($valid) {
        return $data;
    } else {
        return $valid;
    }
}


function getArrayFromFile(string $arrayName)
{

    // Check if the file exist
    if (!file_exists(JSON_FILE_PATH)) {
        return false;
    }

    // Get the file content
    $temp = file_get_contents(JSON_FILE_PATH);

    // Decode raw data to json assoc array format
    $tempArray = json_decode($temp, true);

    // Return array or false
    if (isset($tempArray[$arrayName]) && !empty($tempArray[$arrayName])) {
        return array(
            'array' => $tempArray[$arrayName],
            'file' => $tempArray
        );
    } else {
        return false;
    }
}


function hasEntries(array $dataArray, string $word)
{
    // Verify existence of word, if ok return true
    for ($i = 0; $i < count($dataArray); $i++) {

        // Switch into @var type (string or array)
        switch (getType($dataArray[$i])) {
            case 'string':
                if ($word === $dataArray[$i]) {
                    return true;
                }
                break;

            case 'array':
                // get indexName of entrie according to the araay indexes
                $indexName = array_key_exists('word', $dataArray[$i]) ? 'word' : 'phrase';

                for ($j = 0; $j < count($dataArray[$i]); $j++) {                    
                    if (
                        $word === $dataArray[$i][$indexName]
                    ) {
                        return true;
                    }
                }
                break;

            default:
                return true;
                break;
        }
    }

    // if no return at this point, data has not the word
    return false;
}

/**
 * Save the cleanData into the json file.
 *
 * @param array $cleanData
 * @return void
 */
function saveDataToFile(array $cleanData)
{
    (string)$arrayName = $cleanData["array"];

    // If no values, get out
    if ($cleanData === [] || $cleanData === null || $cleanData === 'undefined') :

        print_r(json_encode(array("message" => "no data to save.")));
        exit();

    else :

        // Check if the file exist
        if (!file_exists(JSON_FILE_PATH)) {
            http_response_code(404);
            exit;
        }

        // Get the file content
        $temp = file_get_contents(JSON_FILE_PATH);

        // Decode raw data to json assoc array format
        $tempArray = json_decode($temp, true);

        // Check if array is not "null"
        if (!$tempArray[$arrayName] || $tempArray[$arrayName] == null) {
            http_response_code(404);
            exit();
        }

        // Check if key exist for has entries function
        $word = array_key_exists('word', $cleanData) ? $cleanData['word'] : $cleanData['phrase'];

        // Check if entries already exist
        if (hasEntries($tempArray[$arrayName], $word)) {
            print_r(json_encode(array("message" => "cette valeur existe déja !")));
            exit();
        }

        $cleanData['created_at'] = date('Y-m-d H:i:s');

        // Push the clean post values into file
        switch ($_POST['word_arrays']) {

            case 'banwords':
            case 'goodwords':
                array_push($tempArray[$arrayName], $cleanData['word']);
                break;

            case 'exceptions':
            case 'expressions':
                unset($cleanData["array"]);
                array_push($tempArray[$arrayName], $cleanData);
                break;
                break;

            default:
                http_response_code(400);
                exit();
                break;
        }

        // Encode array to json & Rewrite the file with new content
        $jsonData = json_encode($tempArray);
        file_put_contents(JSON_FILE_PATH, $jsonData);

        // Success
        print_r(json_encode(array("message" => "ok")));
        exit();

    endif;
}


function deleteDataToFile(array $data)
{
    (string)$arrayName = $data['word_array'];
    (int)$index = $data['index'];

    try {

        // Get the good array from file
        $temp = file_get_contents(JSON_FILE_PATH);
        $tempArray = json_decode($temp, true);

        // If no array, return
        if (!$tempArray[$arrayName] || $tempArray[$arrayName] == null) {
            return false;
        }

        // Delete from index the value (string | array)
        unset($tempArray[$arrayName][$index]);

        // Reindex array
        $tempArray[$arrayName] = array_values($tempArray[$arrayName]);

        // Save new array into file
        $jsonData = json_encode($tempArray);
        file_put_contents(JSON_FILE_PATH, $jsonData);

        // Return answer
        return true;
    } catch (\Throwable $th) {
        //throw $th;
        return false;
    }
}
